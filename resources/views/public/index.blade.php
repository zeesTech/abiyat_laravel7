<!doctype html>
<head lang="en">
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <!-- Font awesome -->
        <script src="https://kit.fontawesome.com/d15b038ad9.js" crossorigin="anonymous"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Custome style sheet -->
        <link rel="stylesheet"  href="public_assets/css/stylecustom.css" type="text/css">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>ABIYAT</title>    
        
        <style>
        .floatLeft {
        display: inline-block;
        }
        
        .shadow-sm-category{
        /* border: 1px solid #000000; */
        border-radius: 50%;
        padding: 10%;
        background-color: #e5efe5;
        }
        
        .categories-section::-webkit-scrollbar {
        width: 0px;
        background: transparent; /*/ make scrollbar transparent / */
        }
        </style>   
    </head>


<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<div class="super_container">
        <!-- Header -->
        <header class="header">
        <!-- Top Bar -->
        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col d-flex flex-row">
                        <div class="top_bar_contact_item">
                            <div class="top_bar_icon"><i class="fab fa-whatsapp"></i><a href="#" class="text-white pl-2 ">+91 9823 132 111</a></div>
                        </div>
                        <div class="top_bar_contact_item">
                            <div class="top_bar_icon"><i class="far fa-envelope"></i><a href="#" class="text-white pl-2 ">contact@abiyat.com</a></div>
                        </div>
                        <div class="top_bar_content ml-auto">
                            <div class="top_bar_menu">
                                <ul class="standard_dropdown top_bar_dropdown">
                                    <li> <a href="#" class="text-white">English<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li><a href="#">Italian</a></li>
                                            <li><a href="#">Spanish</a></li>
                                            <li><a href="#">Japanese</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#" class="text-white">$ US dollar<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li><a href="#">EUR Euro</a></li>
                                            <li><a href="#">GBP British Pound</a></li>
                                            <li><a href="#">JPY Japanese Yen</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="top_bar_user">
                                <div class="user_icon text-white"><i class="far fa-user"></i></div>
                                <div><a href="#" class="text-white">Register</a></div>
                                <div><a href="#" class="text-white">Sign in</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- Header Main -->
        <div class="header_main">
            <div class="container">
                <div class="row menu_trigger">
                    <!-- Logo -->
                    <div class="col-lg-2 col-sm-10 col-10 order-1 menu_trigger">
                        <div class="logo_container menu_burger">
                        <div class="cat_burger menu_burger_inner burger_menu_zee">
                            <span></span><span></span><span></span>
                        </div>
                            <div class="logo " style="display: inline-block;"><a href="#" class="text-white" >Abiyat</a></div>
                        </div>
                    </div> <!-- Search -->
                    <div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
                        <div class="header_search">
                            <div class="header_search_content">
                                <div class="header_search_form_container">
                                    <form action="#" class="header_search_form clearfix"> <input type="search" required="required" class="header_search_input" placeholder="Search for products...">
                                        <div class="custom_dropdown" style="display: none;">
                                            <div class="custom_dropdown_list"> <span class="custom_dropdown_placeholder clc">All Categories</span> <i class="fas fa-chevron-down"></i>
                                                <ul class="custom_list clc">
                                                    <li><a class="clc" href="#">All Categories</a></li>
                                                    <li><a class="clc" href="#">Computers</a></li>
                                                    <li><a class="clc" href="#">Laptops</a></li>
                                                    <li><a class="clc" href="#">Cameras</a></li>
                                                    <li><a class="clc" href="#">Hardware</a></li>
                                                    <li><a class="clc" href="#">Smartphones</a></li>
                                                </ul>
                                            </div>
                                        </div> <button type="submit" class="header_search_button trans_300" value="Submit"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918770/search.png" alt=""></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- Wishlist -->
                    <div class="col-lg-4 col-2 order-lg-3 order-2 text-lg-left text-right">
                        <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
                            <div class="wishlist d-flex flex-row align-items-center justify-content-end">
                                <div class="wishlist_icon"><img src="public_assets/images/wishlisticon.png" alt=""></div>
                                <div class="wishlist_content">
                                    <div class="wishlist_text"><a href="#">Wishlist</a></div>
                                    <div class="wishlist_count">10</div>
                                </div>
                            </div> <!-- Cart -->
                            <div class="cart">
                                <div class="cart_container d-flex flex-row align-items-center justify-content-end">
                                    <div class="cart_icon"> <img src="public_assets/images/carticon.png" alt="">
                                        <div class="cart_count"><span>3</span></div>
                                    </div>
                                    <div class="cart_content">
                                        <div class="cart_text"><a href="#">Cart</a></div>
                                        <div class="cart_price">$185</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- Main Navigation -->
        <nav class="main_nav">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="main_nav_content d-flex flex-row">
                            <!-- Categories Menu -->
                            <!-- Main Nav Menu -->
                            <div class="main_nav_menu">
                                <ul class="standard_dropdown main_nav_dropdown">
                                    <li class="hassubs"> <a href="#" >All Categories<i class="fas fa-caret-down"></i></a>
                                        <ul>
                                            <li><a href="#">Electronics</a></li>
                                            <li><a href="#">Fasion</a></li>
                                            <li><a href="#">Home & Kitchen</a></li>
                                            <li><a href="#">Beauty & Fragrance</a></li>
                                            <li><a href="#">Baby & Toys</a></li>
                                            <li><a href="#">Grocery</a></li>
                                            <li><a href="#">Sports</a></li>
                                            <li><a href="#">Groceries</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="hassubs"> <a href="#">Electronics<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li> <a href="#">Lenovo<i class="fas fa-chevron-down"></i></a>
                                                <ul>
                                                    <li><a href="#">Lenovo 1<i class="fas fa-chevron-down"></i></a></li>
                                                    <li><a href="#">Lenovo 3<i class="fas fa-chevron-down"></i></a></li>
                                                    <li><a href="#">Lenovo 2<i class="fas fa-chevron-down"></i></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">DELL<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="#">APPLE<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="#">HP<i class="fas fa-chevron-down"></i></a></li>
                                        </ul>
                                    </li>
                                    <li class="hassubs"> <a href="#">Fashion<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li> <a href="#">Makeup<i class="fas fa-chevron-down"></i></a>
                                                <ul>
                                                    <li><a href="#">Lipstic<i class="fas fa-chevron-down"></i></a></li>
                                                    <li><a href="#">Eye Liner<i class="fas fa-chevron-down"></i></a></li>
                                                    <li><a href="#">Base<i class="fas fa-chevron-down"></i></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Lipstic<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="#">Eye Liner<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="#">Base<i class="fas fa-chevron-down"></i></a></li>
                                        </ul>
                                    </li>
                                    <li class="hassubs"> <a href="#">Home<i class="fas fa-chevron-down"></i></a>
                                        <ul>
                                            <li><a href="shop.html">Shop<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
                                            <li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
                                        </ul>
                                    </li>
                                    <li class="hassubs"><a href="#">Beauty & Fragrance<i class="fas fa-chevron-down"></i></a></li>
                                    <li class="hassubs"><a href="#">Baby & Toys<i class="fas fa-chevron-down"></i></a></li>
                                    <li class="hassubs"><a href="#">Groceries<i class="fas fa-chevron-down"></i></a></li>
                                    <li class="hassubs"><a href="#">Sports<i class="fas fa-chevron-down"></i></a></li>
                                    <li class="hassubs"><a href="#">Mobiles<i class="fas fa-chevron-down"></i></a></li>
                                </ul>
                            </div> <!-- Menu Trigger -->
                            <!-- <div class="menu_trigger_container ml-auto">
                                <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                                    <div class="menu_burger">
                                        <div class="menu_trigger_text">menu</div>
                                        <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                    </div>
                                </div>
                            </div -->
                        </div>
                    </div>
                </div>
            </div>
        </nav> <!-- Menu -->
        <div class="page_menu">
            <div class="container">
                <div class="row">
                    <div class="col" style="padding-right: 0px;
    padding-left: 0px;">
                        <div class="page_menu_content">
                            <div class="page_menu_search">
                                <form action="#"> <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products..."> </form>
                            </div>
                            <ul class="page_menu_nav">
                                <li class="page_menu_item has-children"> <a href="#">Language<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item has-children"> <a href="#">Currency<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">US Dollar<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">GBP British Pound<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">JPY Japanese Yen<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item"> <a href="#">Home<i class="fa fa-angle-down"></i></a> </li>
                                <li class="page_menu_item has-children"> <a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
                                        <li class="page_menu_item has-children"> <a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
                                            <ul class="page_menu_selection">
                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item has-children"> <a href="#">Featured Brands<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">Featured Brands<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item has-children"> <a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
                                    <ul class="page_menu_selection">
                                        <li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="page_menu_item"><a href="blog.html">blog<i class="fa fa-angle-down"></i></a></li>
                                <li class="page_menu_item"><a href="contact.html">contact<i class="fa fa-angle-down"></i></a></li>
                            </ul>
                            <div class="menu_contact">
                                <div class="menu_contact_item">
                                    <div class="menu_contact_icon"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918577/phone.png" alt=""></div>+38 068 005 3570
                                </div>
                                <div class="menu_contact_item">
                                    <div class="menu_contact_icon"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918577/phone.png" alt=""></div><a href="mailto:fastsales@gmail.com">fastsales@gmail.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </header>
        <div class="container-fluid p-0 bg-light">
            <!--Carousel Open  -->
            <div class="container-fluid p-0 bg-light">
        
                <div id="carouselExampleIndicators" class="carousel slide bg-light " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=" indicator-color active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3" class="indicator-color"></li>
                    </ol>
                    <div class="carousel-inner ">
                    <div class="carousel-item active ">
                        <a href="#"><img src="public_assets/images/Bannernew.png" class="d-block w-100 " style="min-height: 170px;" alt="..."></a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Bannernew.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Bannernew.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Bannernew.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                        </a>
                    </div>
                    </div>
                    <a class="carousel-control-prev " href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            
            </div>
            <!--Carousel Close -->
            <!-- Category Images open-->
            <div class="container-fluid pt-4 bg-white categories-section" style="overflow-x: scroll;overflow-y: hidden;">
                <div class="pl-5 pr-5" style="white-space: nowrap;">
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 shaded-sm bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/APPLIANCES.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>APPLIANCES</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/BABY ESSENTIAL.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption"><b>BABY ESSENTIAL</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/BEAUTY BOUTIQUE.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption"><b>BEAUTY BOUTIQUE</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/BEAUTY.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>BEAUTY</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/ELECTRONICS.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>ELECTRONICS</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/FASHION.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b> FASHION</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/FRAGRANCES.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>FRAGRANCES</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/GROCERIES.png" class="img-fluid shadow-sm-category " alt="">
                                <br>
                                <span class="circle-image-caption "><b>GROCERIES</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/HOME & KITCHEN.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>KICTHEN</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/MOBILES.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>MOBILES</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/SPORTS & OUTDOOR.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>SPORTS</b></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                        <div class="m-1 bg-white">
                            <a href="#" class="text-decoration-non text-dark ">
                                <img src="public_assets/images/circle/TOYS & GAMES.png" class="img-fluid shadow-sm-category" alt="">
                                <br>
                                <span class="circle-image-caption "><b>TOYS & GAMES</b></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Category Images closed -->

            <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid pl-5 pr-5 bg-light">
                <div class="pt-5">
                    <h5><b>Recommended For You</b></h5>
                </div>
                <div class="row ">
                    <div class="col">
                        <div id="carouselProductIndicators1" class="carousel slide  " data-ride="carousel">
                            <div class="carousel-inner carousel-inner-product justify-content-center">
                                <!-- carousel item 1 open -->
                                <div class="carousel-item active ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1 top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 1 close -->

                                <!-- carousel item 2 open -->
                                <div class="carousel-item ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 2 close -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselProductIndicators1" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselProductIndicators1" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- Product Carousel close-->         
            
            <!-- Add with Code Open-->
            <!-- open-->
            <div class="container-fluid bg-light p-5">
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/add rectangle.gif"class=" img-fluid" alt=""></a>
                    </div>   
                </div>
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/add rectangle small.png"class=" img-fluid" alt=""></a>
                    </div>   
                </div>
                <div class="row bg-transparent m-5">
                    <div class="col-sm-6 col-md-3 ">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 mb-3 "><img src="public_assets/images/circle/MOBILES.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                            </a>
                            <h5><b>Get 10% off on all Mobile & Accessories</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 ">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/SPORTS & OUTDOOR.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                            </a>
                            <h5><b>Get 10% off on all Sports Products</b></h5> 
                        </div>      
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/HOME & KITCHEN.png"class="rounded mb-3 shadow-sm img-fluid" style="background-color: #9ec1a0;" alt="">
                            </a>
                            <h5><b>Get 10% off on all Kitchen Electronics</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/BEAUTY BOUTIQUE.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                            </a>
                            <h5><b>Get 10% off on all Fashion & Cosmetics</b></h5>
                        </div>
                    </div>   
                </div>
            </div>
            <!-- opne-->
            <!-- Add with Code Close-->


            <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid pl-5 pr-5 bg-light">
                <div class="">
                    <h5><b>Recommended For You</b></h5>
                </div>
                <div class="row ">
                    <div class="col">
                        <div id="carouselProductIndicators2" class="carousel slide  " data-ride="carousel">
                            <div class="carousel-inner carousel-inner-product justify-content-center">
                                <!-- carousel item 1 open -->
                                <div class="carousel-item active ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class=" rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 1 close -->

                                <!-- carousel item 2 open -->
                                <div class="carousel-item ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 2 close -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselProductIndicators2" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselProductIndicators2" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            
                            </div>
                    </div>
                </div>

            </div>
            <!-- Product Carousel close-->


            <!-- Add Mega Deal Open -->
            <div class="container-fluid p-5">
                <div class="row ">
                    <div class="">
                        <h5><b>MEGA DEALS</b></h5>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-6 col-md-3 ">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-6 col-md-3">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-6 col-md-3">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-6 col-md-3">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <!-- Add Mega Deal Open -->

            <!--Carousel Open  -->
            <div class="container-fluid p-0 mt-5 bg-light">
        
                <div id="carouselExampleIndicators1" class="carousel slide bg-light " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class=" indicator-color active"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="1" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="2" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="3" class="indicator-color"></li>
                    </ol>
                    <div class="carousel-inner ">
                        <div class="carousel-item active ">
                            <a href="#"><img src="public_assets/images/Banner 1.png" class="d-block w-100 " style="min-height: 170px;"  alt="..."></a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;"alt="...">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev " href="#carouselExampleIndicators1" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleIndicators1" role="button" data-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!--Carousel Close -->

            <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid pt-5 pl-5 pr-5 bg-light">
                <div>
                    <h5><b>Recommended For You</b></h5>
                </div>
                <div class="row ">
                    <div class="col">
                        <div id="carouselProductIndicators3" class="carousel slide  " data-ride="carousel">
                            <div class="carousel-inner carousel-inner-product justify-content-center">
                                <!-- carousel item 1 open -->
                                <div class="carousel-item active ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 1 close -->

                                <!-- carousel item 2 open -->
                                <div class="carousel-item ">
                                    <!-- product Carousel -->
                                    <div class="container-fluid p-0 bg-light">
                                        <div class="row ">
                                            <!-- Product 1 Open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 1 close -->
                                            <!-- Product 2 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 2 close -->
                                            <!-- Product 3 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 3 close -->
                                            <!-- Product 4 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 4 close -->
                                            <!-- Product 5 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 5 close -->
                                            <!-- Product 6 open -->
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 " style="width: auto; height:auto">
                                                <div style="width:auto; height:fit-content; max-width: 155px;">
                                                    <a href="#" class="text-decoration-non text-dark m-2">
                                                        <div class="  rounded-lg ml-1 mr-1  top-discount-bar-color text-truncate">
                                                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                                                        </div>    
                                                        <div class="p-1">
                                                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                                                        </div >
                                                        <div class="ml-2 mr-2">
                                                            <span class="product-detail product-description" >get 10% discount</span>
                                                        </div>
                                                        <div class="from-inline pl-2 pr-2">
                                                            <span class="text-muted currency-name" >AED</span>
                                                            <span class="currecny-value"><b>19.50</b></span>
                                                            <span class="currency-line">AED</span>
                                                            <span class="value-line">57</span>
                                                        </div>
                                                        <div class="form-inline pl-2 p-0">
                                                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Product 6 close -->
                                        </div>
                                    </div>
                                    <!-- Product carousel close -->
                                </div>
                                <!-- carousel item 2 close -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselProductIndicators3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselProductIndicators3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            
                            </div>
                    </div>
                </div>

            </div>
            <!-- Product Carousel close-->

            <!-- Featured Brand open-->
            <div class="container-fluid bg-white pt-5 pl-5 pr-5">
                <div class="d-flex justify-content-between">
                    <div class="">
                        <h5><b>FEATURED BRAND</b></h5>
                    </div>
                    <div class="">
                        <button type="button" class="btn btn-light btn-sm border">View All</button>
                    </div>
                </div>
                <div class="row bg-transparent">
                    <div class="col-3 col-md-2 p-2 "  >
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;" ><img src="public_assets/images/FEATURED BRAND.png"class="img-fluid" alt=""></a>
                    </div>
                    <div class="col-3 col-md-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>       
                    </div>
                    <div class="col-3 col-md-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-3 col-md-2 p-2">
                        <a href="#" class="dropdown-item"style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-3 col-md-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-3 col-md-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>  
                </div>
                <div class="row bg-transparent">
                    <div class="col-2 p-2 ">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>       
                    </div>
                    <div class="col-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>
                    <div class="col-2 p-2">
                        <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                    </div>    
                </div>
            </div>
            <!-- Featured Brand close-->
            
            <!-- open-->
            <div class="container-fluid bg-white p-5">
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/Banner 2.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>   
                </div>
                <div class="row bg-transparent">
                    <div class="col-sm-6 col-md-3 p-2 ">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/MOBILES.png"class="rounded shadow-sm img-fluid" alt="">
                            </a><br>
                            <h5><b>MOBILES</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 p-2">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/SPORTS & OUTDOOR.png"class="rounded shadow-sm img-fluid" alt="">
                            </a><br>
                            <h5><b>SPORTS</b></h5> 
                        </div>      
                    </div>
                    <div class="col-sm-6 col-md-3 p-2">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/HOME & KITCHEN.png"class="rounded shadow-sm img-fluid" alt="">
                            </a><br>
                            <h5><b>KITCHEN ELECTRONICS</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 p-2">
                        <div class="text-center">
                            <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/GROCERIES.png"class="rounded shadow-sm img-fluid" alt="">
                            </a><br>
                            <h5><b>GROSERIES</b></h5>
                        </div>
                    </div>   
                </div>
            </div>
            <!-- opne-->
        </div>
    </div>


    <script>
    $(document).ready(function()
    {
    "use strict";

    var menuActive = false;
    var header = $('.header');
    setHeader();
    initCustomDropdown();
    initPageMenu();

    function setHeader()
    {

    if(window.innerWidth > 991 && menuActive)
    {
    closeMenu();
    }
    }

    function initCustomDropdown()
    {
    if($('.custom_dropdown_placeholder').length && $('.custom_list').length)
    {
    var placeholder = $('.custom_dropdown_placeholder');
    var list = $('.custom_list');
    }

    placeholder.on('click', function (ev)
    {
    if(list.hasClass('active'))
    {
    list.removeClass('active');
    }
    else
    {
    list.addClass('active');
    }

    $(document).one('click', function closeForm(e)
    {
    if($(e.target).hasClass('clc'))
    {
    $(document).one('click', closeForm);
    }
    else
    {
    list.removeClass('active');
    }
    });

    });

    $('.custom_list a').on('click', function (ev)
    {
    ev.preventDefault();
    var index = $(this).parent().index();

    placeholder.text( $(this).text() ).css('opacity', '1');

    if(list.hasClass('active'))
    {
    list.removeClass('active');
    }
    else
    {
    list.addClass('active');
    }
    });


    $('select').on('change', function (e)
    {
    placeholder.text(this.value);

    $(this).animate({width: placeholder.width() + 'px' });
    });
    }

    /*

    4. Init Page Menu

    */

    function initPageMenu()
    {
    if($('.page_menu').length && $('.page_menu_content').length)
    {
    var menu = $('.page_menu');
    var menuContent = $('.page_menu_content');
    var menuTrigger = $('.menu_trigger');

    //Open / close page menu
    menuTrigger.on('click', function()
    {
    if(!menuActive)
    {
    openMenu();
	return false;
    }
    else
    {
    closeMenu();
	return false;
    }
    });

    //Handle page menu
    if($('.page_menu_item').length)
    {
    var items = $('.page_menu_item');
    items.each(function()
    {
    var item = $(this);
    if(item.hasClass("has-children"))
    {
    item.on('click', function(evt)
    {
    evt.preventDefault();
    evt.stopPropagation();
    var subItem = item.find('> ul');
    if(subItem.hasClass('active'))
    {
    subItem.toggleClass('active');
    TweenMax.to(subItem, 0.3, {height:0});
    }
    else
    {
    subItem.toggleClass('active');
    TweenMax.set(subItem, {height:"auto"});
    TweenMax.from(subItem, 0.3, {height:0});
    }
    });
    }
    });
    }
    }
    }

    function openMenu()
    {
    var menu = $('.page_menu');
    var menuContent = $('.page_menu_content');
    TweenMax.set(menuContent, {height:"auto"});
    TweenMax.from(menuContent, 0.3, {height:0});
    menuActive = true;
    }

    function closeMenu()
    {
    var menu = $('.page_menu');
    var menuContent = $('.page_menu_content');
    TweenMax.to(menuContent, 0.3, {height:0});
    menuActive = false;
    }


    });
    </script>
    
</body>
</html>



